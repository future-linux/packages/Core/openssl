# Maintainer: Future Linux Team <future_linux@163.com>

pkgname=openssl
pkgver=3.0.5
pkgrel=1
pkgdesc='The Open Source toolkit for Secure Sockets Layer and Transport Layer Security'
arch=('x86_64')
url='https://www.openssl.org'
license=('custom:BSD')
groups=('base')
depends=('glibc' 'perl')
makedepends=('binutils' 'coreutils' 'gcc' 'make')
backup=(etc/ssl/openssl.cnf)
source=(https://www.openssl.org/source/${pkgname}-${pkgver}.tar.gz)
sha256sums=(aa7d8d9bef71ad6525c55ba11e5f4397889ce49c2c9349dcea6d3e4f0b024a7a)

prepare() {
    cd ${pkgname}-${pkgver}

    sed -e '/bn_reduce.*m1/i\    factor_size /= sizeof(BN_ULONG) * 8;' \
        -i crypto/bn/rsaz_exp_x2.c
}

build() {
    cd ${pkgname}-${pkgver}

    ./config                  \
        --prefix=/usr         \
        --openssldir=/etc/ssl \
        --libdir=lib64        \
        shared                \
        zlib-dynamic

    make
}

package() {
    cd ${pkgname}-${pkgver}

    sed -i '/INSTALL_LIBS/s/libcrypto.a libssl.a//' Makefile

    make DESTDIR=${pkgdir} install
}
